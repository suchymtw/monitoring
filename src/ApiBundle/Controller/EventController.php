<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Form\EventType;
use AppBundle\Service\ActionManager;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * Class EventController
 *
 * @package ApiBundle\Controller
 */
class EventController extends Controller
{
    /**
     * @Post("/new")
     */
    public function newEventAction(Request $request)
    {
        $event = new Event();
        $actionManager = $this->getActionManager();

        $form = $this->createForm(EventType::class);
        $form->submit($request);

        if ($form->isValid()) {
            $success = $actionManager->handleEvent($event);
            $status = ($success)? Response::HTTP_OK : Response::HTTP_BAD_REQUEST;
            return View::create([], $status);
        }

        return View::create(['form' => $form], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return ActionManager
     */
    protected function getActionManager()
    {
        return $this->get('action_manager');
    }
}
