<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ActionForEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Actionforevent controller.
 *
 * @Route("actions")
 */
class ActionForEventController extends Controller
{
    /**
     * Lists all actionForEvent entities.
     *
     * @Route("/", name="actions_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actionForEvents = $em->getRepository('AppBundle:ActionForEvent')->findAll();

        return $this->render('actionforevent/index.html.twig', array(
            'actionForEvents' => $actionForEvents,
        ));
    }

    /**
     * Creates a new actionForEvent entity.
     * @todo add param type event and check event type from Event entity
     * @see Event
     * @Route("/new", name="actions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $actionForEvent = new ActionForEvent();
        $form = $this->createForm('AppBundle\Form\ActionForEventType', $actionForEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($actionForEvent);
            $em->flush();

            return $this->redirectToRoute('actions_show', array('id' => $actionForEvent->getId()));
        }

        return $this->render('actionforevent/new.html.twig', array(
            'actionForEvent' => $actionForEvent,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a actionForEvent entity.
     *
     * @Route("/{id}", name="actions_show")
     * @Method("GET")
     */
    public function showAction(ActionForEvent $actionForEvent)
    {
        $deleteForm = $this->createDeleteForm($actionForEvent);

        return $this->render('actionforevent/show.html.twig', array(
            'actionForEvent' => $actionForEvent,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing actionForEvent entity.
     * @todo add param type event and check event type from Event entity
     * @see Event
     *
     * @Route("/{id}/edit", name="actions_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ActionForEvent $actionForEvent)
    {
        $deleteForm = $this->createDeleteForm($actionForEvent);
        $editForm = $this->createForm('AppBundle\Form\ActionForEventType', $actionForEvent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('actions_edit', array('id' => $actionForEvent->getId()));
        }

        return $this->render('actionforevent/edit.html.twig', array(
            'actionForEvent' => $actionForEvent,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a actionForEvent entity.
     *
     * @Route("/{id}", name="actions_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ActionForEvent $actionForEvent)
    {
        $form = $this->createDeleteForm($actionForEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($actionForEvent);
            $em->flush();
        }

        return $this->redirectToRoute('actions_index');
    }

    /**
     * Creates a form to delete a actionForEvent entity.
     *
     * @param ActionForEvent $actionForEvent The actionForEvent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ActionForEvent $actionForEvent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('actions_delete', array('id' => $actionForEvent->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
