<?php
namespace AppBundle\Service;

use AppBundle\Entity\Event;
use AppBundle\Service\Action\ActionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractActionManager
 *
 * @package AppBundle\Service
 */
class AbstractActionManager
{

    /**
     * @var ActionInterface
     */
    private $action;

    /**
     * @var Event
     */
    protected $event;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ActionManager constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {

    }

    /**
     * @return ActionInterface
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param ActionInterface $action
     *
     * @return ActionManager
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     *
     * @return ActionManager
     */
    public function setEvent($event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return ActionManager
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }
}