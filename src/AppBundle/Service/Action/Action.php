<?php
namespace AppBundle\Service\Action;

use AppBundle\Entity\Event;

/**
 * Class Action
 *
 * @package AppBundle\Service\Action
 */
class Action implements ActionInterface
{

    /**
     * @var ActionInterface
     */
    protected $action;

    /**
     * Action constructor.
     *
     * @param ActionInterface $action
     */
    public function __construct(ActionInterface $action)
    {
        $this->action = $action;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->action->setSettings($settings);
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event)
    {
        $this->action->setEvent($event);
    }

    /**
     * @return bool
     */
    public function process():bool
    {
        return $this->action->process();
    }
}