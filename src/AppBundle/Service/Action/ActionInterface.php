<?php
namespace AppBundle\Service\Action;

use AppBundle\Entity\Event;

/**
 * Interface ActionInterface
 *
 * @package AppBundle\Service\Action
 */
interface ActionInterface
{
    /**
     * @param array $settings
     *
     * @return mixed
     */
    public function setSettings(array $settings);

    /**
     * @param Event $event
     *
     * @return mixed
     */
    public function setEvent(Event $event);

    /**
     * @return bool
     */
    public function process():bool;
}