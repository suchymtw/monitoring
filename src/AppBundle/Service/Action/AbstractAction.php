<?php
namespace AppBundle\Service\Action;

use AppBundle\Entity\Event;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractAction
 *
 * @package AppBundle\Service\Action
 */
abstract class AbstractAction implements ActionInterface
{
    /**
     * @var array
     */
    protected $settings = [];
    /**
     * @var Event
     */
    protected $event;
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AbstractAction constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     *
     * @return AbstractAction
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     *
     * @return AbstractAction
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return AbstractAction
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }
}