<?php
namespace AppBundle\Service;

use AppBundle\Entity\ActionForEvent;
use AppBundle\Service\Action as Actions;
use AppBundle\Service\Action\ActionInterface;

/**
 * Class ActionManager
 *
 * @package AppBundle\Service
 */
class ActionManager extends AbstractActionManager
{

    /**
     * @param $event
     */
    public function handleEvent($event)
    {
        $actions = $this->getActionsForEvent($event);

        /**
         * @var ActionForEvent $action
         */
        foreach ($actions as $action) {
            $this->process($action->getActionName(), $action->getSettings());
        }
    }

    /**
     * @param $actionName
     * @param array $settings
     *
     * @return bool
     */
    protected function process($actionName, array $settings):bool
    {
        try {
            $actionInstance = $this->getActionInstance($actionName);
            $action = new Actions\Action($actionInstance);
            $action->setEvent($this->getEvent());
            $action->setSettings($settings);
            return $action->process();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $event
     *
     * @return mixed
     */
    protected function getActionsForEvent($event):array
    {
        //get all actions for event form database
    }

    /**
     * @param $actionName
     *
     * @return ActionInterface
     * @throws \Exception
     */
    protected function getActionInstance($actionName):ActionInterface
    {
        if ($this->checkActionExists($actionName)) {
            $className = $this->createClassName($actionName);
            return new $className($this->getContainer()); //this is the simplest way to create action instance but use ReflectionClass is a better way
        }
        throw  new \Exception('Invalid action:' . $actionName);
    }

    /**
     * @param $actionName
     *
     * @return bool
     */
    protected function checkActionExists($actionName):bool
    {
        //checking action exists
    }

    /**
     * @param $actionName
     *
     * @return string
     */
    protected function createClassName($actionName):string
    {
        // Returns classname with namespace for example: Actions/LogAction
    }
}