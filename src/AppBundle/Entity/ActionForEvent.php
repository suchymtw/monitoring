<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActionForEvent
 * @todo add validator for field action
 * @ORM\Table(name="action_for_event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActionForEventRepository")
 */
class ActionForEvent
{

    const ACTION_LOG = 1;
    const ACTION_MAIL = 2;
    const ACTION_SMS = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="event_level", type="smallint")
     */
    private $eventLevel;

    /**
     * @var int
     *
     * @ORM\Column(name="action", type="smallint")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="settings", type="string", length=255, nullable=true)
     */
    private $settings;


    public function getActionName()
    {
        //returns prepared action name from const by id eg id is 1, function returns "Log"
        //when const is not defined returns first action name
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventLevel
     *
     * @param integer $eventLevel
     *
     * @return ActionForEvent
     */
    public function setEventLevel($eventLevel)
    {
        $this->eventLevel = $eventLevel;

        return $this;
    }

    /**
     * Get eventLevel
     *
     * @return int
     */
    public function getEventLevel()
    {
        return $this->eventLevel;
    }

    /**
     * Set action
     *
     * @param integer $action
     *
     * @return ActionForEvent
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set settings
     *
     * @param string $settings
     *
     * @return ActionForEvent
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return string
     */
    public function getSettings()
    {
        return $this->settings;
    }
}

